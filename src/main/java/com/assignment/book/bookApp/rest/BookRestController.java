package com.assignment.book.bookApp.rest;

import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.book.bookApp.entity.Book;
import java.util.List;

@RestController
@RequestMapping("/api")
public class BookRestController {
	

		private List<Book> theBooks;
		
		
		//this annotation is called only once during the bean creation
		@PostConstruct
		public void loadData()
		{
			theBooks = new ArrayList<>();
			theBooks.add(new Book(0,"Book1","Science Fiction"));
			theBooks.add(new Book(1,"Book2","Drama"));
			theBooks.add(new Book(2,"Book3","Non fiction"));
			theBooks.add(new Book(3,"Book4","Thriller"));
			theBooks.add(new Book(4,"Book5","Romance"));
			
		}
		
		@GetMapping("/books")
		public List<Book> getBooks()
		{
			
			
			return theBooks;
			
		}
		
		@GetMapping("/books/{bookId}")
		public Book getBook(@PathVariable int bookId)
		{	
			if(bookId>=theBooks.size() || bookId<0) {
				throw new BookNotFoundException("Book Id not found - "+bookId);
			}
				
			return theBooks.get(bookId);
			
		}
		
		
		@PostMapping("/books")
		public Book addBook(@RequestBody Book theBook)
		{
			theBook.setId(theBooks.size());
			theBooks.add(theBook);
			
				
			return theBook;
			
		}
		
		@PutMapping("/books")
		public Book updateBook(@RequestBody Book theBook)
		{
			for(Book b:theBooks)
			{
				if(b.getId()==theBook.getId())
				{
					b.setName(theBook.getName());
					b.setDescription(theBook.getDescription());
				}
			}
			
				
			return theBook;
			
		}
		
		

}
