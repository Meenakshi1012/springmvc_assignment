
*bookApp is a spring boot project which has maven dependecy for Tomcat sever.
*Import the project bookApp into a workspace.
*Run the file BookAppApplication

*Client Tool Used to test is Postman
*Run the following commands in postman to test the APIs.
*[To get the list of books in the system](GET http://localhost:8080/api/books/)

*[To get a book by its ID](GET http://localhost:8080/api/books/(**any int number)

*[To add a new entry into the list]
(POST http://localhost:8080/api/books/)
*BODY of the request must be in the JSON format
{
"name":"Book6",
	"description":"Romance Drama"
}

*[To update an existing entry in the list]
(PUT http://localhost:8080/api/books/)
*BODY of the request must be in the JSON format
{"id":1,
"name":"any new name",
	"description":"any new description"
}

